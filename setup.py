#!/usr/bin/env python

from setuptools import setup, find_packages

setup(name='baste',
    version='1.0',
    description='Baste provide extensions for Fabric',
    author='Lakin Wecker',
    author_email='lakin@structuredabstraction.com',
    url='https://bitbucket.org/lakin.wecker/baste',
    setup_requires=['setuptools_scm'],
    packages=find_packages()
)


